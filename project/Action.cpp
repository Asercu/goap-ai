#include "stdafx.h"
#include "Action.h"


Action::Action()
{
	m_pRequirements = new vector<WorldStateProperty*>();
	m_pEffects = new vector<WorldStateProperty*>();
}


Action::~Action()
{
	for (unsigned int i = 0; i < m_pRequirements->size(); ++i)
	{
		delete m_pRequirements->at(i);
	}
	delete m_pRequirements;
	for (unsigned int i = 0; i < m_pEffects->size(); ++i)
	{
		delete m_pEffects->at(i);
	}
	delete m_pEffects;
}

void Action::AddRequirement(WorldStateProperty* requirement)
{
	m_pRequirements->push_back(requirement);
}

void Action::AddEffect(WorldStateProperty* effect)
{
	m_pEffects->push_back(effect);
}

void Action::Execute()
{
	m_Function();
}
