#include "stdafx.h"
#include "Plugin.h"
#include "IExamInterface.h"

//Called only once, during initialization
void Plugin::Initialize(IBaseInterface* pInterface, PluginInfo& info)
{
	//Retrieving the interface
	//This interface gives you access to certain actions the AI_Framework can perform for you
	m_pInterface = static_cast<IExamInterface*>(pInterface);

	//Bit information about the plugin
	//Please fill this in!!
	info.BotName = "Goapzilla";
	info.Student_FirstName = "Arne";
	info.Student_LastName = "Sercu";
	info.Student_Class = "2DAE5";
}

//Called only once
void Plugin::DllInit()
{
	//Can be used to figure out the source of a Memory Leak
	//Possible undefined behavior, you'll have to trace the source manually 
	//if you can't get the origin through _CrtSetBreakAlloc(0) [See CallStack]
	//_CrtSetBreakAlloc(375735);

	//Called when the plugin is loaded
	//create all GOAP worldstate variables
	//Full worldstate for bot
	m_pWorldState = new WorldState();
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::HasFood, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::FoodAmount, 0));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::Currenthealth, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::MaxHealth, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::HasMedkit, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::MedKitAmount, 0));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::CurrentEnergy, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::MaxEnergy, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::CurrentStamina, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::MaxStamina, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::HasGun, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::SeesEnemy, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::SeesItem, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::SeesHouse, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::IsInHouse, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::IsHoldingItem, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::isEnemyInRange, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::IsitemInRange, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::GunRange, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::ShoudEat, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::ShouldHeal, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::EnemyInFront, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::LinearVelocity, {0,0}));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::AngularVelocity, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::CurrentLinearSpeed, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::Position, {0,0}));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::Orientation, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::MaxLinearSpeed, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::MaxAngularSpeed, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::GrabRange, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::AgentSize, 0.f));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::IsBitten, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::CheckpointLocation, { 0,0 }));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::NearestEnemyPosition, { 0,0 }));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::NearestItemPosition, { 0,0 }));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::hasHealed, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::hasEaten, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::HasAttacked, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::itemCollected, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::checkPointgathered, false));
	m_pWorldState->AddProperty(new WorldStateProperty(WorldStatePropertyKey::NearestEnemySize, 0.f));

	//goals
	m_pGoals = vector<Goal*>{};
	Goal* healed = new Goal();
	healed->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::hasHealed, true, true));
	m_pGoals.push_back(healed);
	Goal* eaten = new Goal();
	eaten->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::hasEaten, true, true));
	m_pGoals.push_back(eaten);
	Goal* attacked = new Goal();
	attacked->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::HasAttacked, true, true));
	m_pGoals.push_back(attacked);
	Goal* collectedItem = new Goal();
	collectedItem->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::itemCollected, true, true));
	m_pGoals.push_back(collectedItem);
	Goal* collectedCheckPoint = new Goal();
	collectedCheckPoint->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::checkPointgathered, true, true));
	m_pGoals.push_back(collectedCheckPoint);

	//planner
	m_pPlanner = new Planner();

	//add actions
	Action* heal = new Action();
	heal->AddEffect(new WorldStateProperty(WorldStatePropertyKey::hasHealed, true, true));
	heal->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::HasMedkit, true, true));
	heal->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::ShouldHeal, true, true));
	heal->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::hasHealed, false, true));
	heal->m_Function = std::bind(&Plugin::Heal, this);
	m_pPlanner->AddAction(heal);

	Action* eat = new Action();
	eat->AddEffect(new WorldStateProperty(WorldStatePropertyKey::hasEaten, true, true));
	eat->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::HasFood, true, true));
	eat->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::ShoudEat, true, true));
	eat->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::hasEaten, false, true));
	eat->m_Function = std::bind(&Plugin::Eat, this);
	m_pPlanner->AddAction(eat);

	Action* attack = new Action();
	attack->AddEffect(new WorldStateProperty(WorldStatePropertyKey::HasAttacked, true, true));
	attack->AddRequirement((new WorldStateProperty(WorldStatePropertyKey::HasGun, true, true)));
	attack->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::EnemyInFront, true, true));
	attack->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::isEnemyInRange, true, true));
	attack->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::HasAttacked, false, true));
	attack->m_Function = std::bind(&Plugin::Attack, this);
	m_pPlanner->AddAction(attack);

	Action* collect = new Action();
	collect->AddEffect(new WorldStateProperty(WorldStatePropertyKey::itemCollected, true, true));
	collect->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::IsHoldingItem, true, true));
	collect->m_Function = std::bind(&Plugin::Collect, this);
	m_pPlanner->AddAction(collect);
;
	Action* gatherCheckpoint = new Action();
	gatherCheckpoint->AddEffect(new WorldStateProperty(WorldStatePropertyKey::checkPointgathered, true, true));
	gatherCheckpoint->m_Function = std::bind(&Plugin::GatherCheckpoint, this);
	m_pPlanner->AddAction(gatherCheckpoint);

	Action* lookAtEnemy = new Action();
	lookAtEnemy->AddEffect(new WorldStateProperty(WorldStatePropertyKey::EnemyInFront, true, true));
	lookAtEnemy->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::SeesEnemy, true, true));
	lookAtEnemy->m_Function = std::bind(&Plugin::LookAtEnemy, this);
	m_pPlanner->AddAction(lookAtEnemy);

	Action* moveToItem = new Action();
	moveToItem->AddEffect(new WorldStateProperty(WorldStatePropertyKey::IsitemInRange, true, true));
	moveToItem->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::SeesItem, true, true));
	moveToItem->m_Function = std::bind(&Plugin::MoveToItem, this);
	m_pPlanner->AddAction(moveToItem);

	Action* moveToEnemy = new Action();
	moveToEnemy->AddEffect(new WorldStateProperty(WorldStatePropertyKey::isEnemyInRange, true, true));
	moveToEnemy->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::HasGun, true, true));
	moveToEnemy->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::SeesEnemy, true, true));
	moveToEnemy->m_Function = std::bind(&Plugin::MoveToEnemy, this);
	m_pPlanner->AddAction(moveToEnemy);

	Action* grabItem = new Action();
	grabItem->AddEffect(new WorldStateProperty(WorldStatePropertyKey::IsHoldingItem, true, true));
	grabItem->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::IsitemInRange, true, true));
	grabItem->AddRequirement(new WorldStateProperty(WorldStatePropertyKey::SeesItem, true, true));
	grabItem->m_Function = std::bind(&Plugin::GrabItem, this);
	m_pPlanner->AddAction(grabItem);
}

//Called only once
void Plugin::DllShutdown()
{
	//Called wheb the plugin gets unloaded
	delete m_pPlanner;
	delete m_pWorldState;
	for (unsigned int i = 0; i < m_pGoals.size(); ++i)
	{
		delete m_pGoals[i];
	}
}

//Called only once, during initialization
void Plugin::InitGameDebugParams(GameDebugParams& params)
{
	params.AutoFollowCam = true; //Automatically follow the AI? (Default = true)
	params.RenderUI = true; //Render the IMGUI Panel? (Default = true)
	params.SpawnEnemies = true; //Do you want to spawn enemies? (Default = true)
	params.EnemyCount = 20; //How many enemies? (Default = 20)
	params.GodMode = false; //GodMode > You can't die, can be usefull to inspect certain behaviours (Default = false)
							//params.LevelFile = "LevelTwo.gppl";
	params.AutoGrabClosestItem = false; //A call to Item_Grab(...) returns the closest item that can be grabbed. (EntityInfo argument is ignored)
	params.OverrideDifficulty = false; //Override Difficulty?
	params.Difficulty = 1.f; //Difficulty Override: 0 > 1 (Overshoot is possible, >1)
}

//Only Active in DEBUG Mode
//(=Use only for Debug Purposes)
void Plugin::ProcessEvents(const SDL_Event& e)
{
	//Demo Event Code
	//In the end your AI should be able to walk around without external input
	switch (e.type)
	{
	case SDL_MOUSEBUTTONUP:
	{
		if (e.button.button == SDL_BUTTON_LEFT)
		{
			int x, y;
			SDL_GetMouseState(&x, &y);
			const Elite::Vector2 pos = Elite::Vector2(static_cast<float>(x), static_cast<float>(y));
			//m_Target = m_pInterface->Debug_ConvertScreenToWorld(pos);
		}
		break;
	}
	case SDL_KEYDOWN:
	{
		if (e.key.keysym.sym == SDLK_SPACE)
		{
			//m_CanRun = true;
		}
		else if (e.key.keysym.sym == SDLK_LEFT)
		{
			//m_AngSpeed -= Elite::ToRadians(10);
		}
		else if (e.key.keysym.sym == SDLK_RIGHT)
		{
			//m_AngSpeed += Elite::ToRadians(10);
		}
		else if (e.key.keysym.sym == SDLK_g)
		{
			//m_GrabItem = true;
		}
		else if (e.key.keysym.sym == SDLK_u)
		{
			//m_UseItem = true;
		}
		else if (e.key.keysym.sym == SDLK_r)
		{
			//m_RemoveItem = true;
		}
		else if (e.key.keysym.sym == SDLK_d)
		{
			//m_DropItem = true;
		}
		break;
	}
	case SDL_KEYUP:
	{
		if (e.key.keysym.sym == SDLK_SPACE)
		{
			//m_CanRun = false;
		}
		break;
	}
	}
}

//Update
//This function calculates the new SteeringOutput, called once per frame
SteeringPlugin_Output Plugin::UpdateSteering(float dt)
{
	timer += dt;
	//reset output
	m_Output = SteeringPlugin_Output();
	m_MustReturn = false;

	//update current state
	auto agentInfo = m_pInterface->Agent_GetInfo();
	if (!m_MaxValuesInitialized) SetMaxValues(agentInfo);
	m_pWorldState->SetProperty(WorldStatePropertyKey::CurrentStamina, agentInfo.Stamina);
	m_pWorldState->SetProperty(WorldStatePropertyKey::Currenthealth, agentInfo.Health);
	m_pWorldState->SetProperty(WorldStatePropertyKey::CurrentEnergy, agentInfo.Energy);
	m_pWorldState->SetProperty(WorldStatePropertyKey::IsInHouse, agentInfo.IsInHouse);
	m_pWorldState->SetProperty(WorldStatePropertyKey::LinearVelocity, agentInfo.LinearVelocity);
	m_pWorldState->SetProperty(WorldStatePropertyKey::AngularVelocity, agentInfo.AngularVelocity);
	m_pWorldState->SetProperty(WorldStatePropertyKey::CurrentLinearSpeed, agentInfo.CurrentLinearSpeed);
	m_pWorldState->SetProperty(WorldStatePropertyKey::Position, agentInfo.Position);
	m_pWorldState->SetProperty(WorldStatePropertyKey::Orientation, agentInfo.Orientation);
	m_pWorldState->SetProperty(WorldStatePropertyKey::MaxLinearSpeed, agentInfo.MaxLinearSpeed);
	m_pWorldState->SetProperty(WorldStatePropertyKey::MaxAngularSpeed, agentInfo.MaxAngularSpeed);
	m_pWorldState->SetProperty(WorldStatePropertyKey::GrabRange, agentInfo.GrabRange);
	m_pWorldState->SetProperty(WorldStatePropertyKey::AgentSize, agentInfo.AgentSize);
	if (!m_CheckpointPosOverride) m_pWorldState->SetProperty(WorldStatePropertyKey::CheckpointLocation, m_pInterface->World_GetCheckpointLocation());
	m_pWorldState->SetProperty(WorldStatePropertyKey::hasHealed, false);
	m_pWorldState->SetProperty(WorldStatePropertyKey::hasEaten, false);
	m_pWorldState->SetProperty(WorldStatePropertyKey::HasAttacked, false);
	m_pWorldState->SetProperty(WorldStatePropertyKey::itemCollected, false);
	m_pWorldState->SetProperty(WorldStatePropertyKey::IsBitten, agentInfo.Bitten);
	m_pWorldState->SetProperty(WorldStatePropertyKey::checkPointgathered, false);
	auto vHousesInFOV = GetHousesInFOV();
	m_pWorldState->SetProperty(WorldStatePropertyKey::SeesHouse, !vHousesInFOV.empty());
	for (const HouseInfo hi : vHousesInFOV)
	{
		if (find(m_Houses.begin(), m_Houses.end(), hi.Center) == m_Houses.end())
		{
			m_Houses.push_back(hi.Center);
			m_NotPassedHouses.push_back(hi.Center);
		}
	}
	auto vEntitiesInFOV = GetEntitiesInFOV();
	bool seesEnemy = false;
	bool seesItem = false;
	vector<EnemyInfo> vEnemiesInFOV{};
	vector<EntityInfo> vItemsInFOV{};
	for (auto entity : vEntitiesInFOV)
	{
		switch (entity.Type)
		{
		case eEntityType::ENEMY:
			{
				seesEnemy = true;
				EnemyInfo enemy = {};
				m_pInterface->Enemy_GetInfo(entity, enemy);
				vEnemiesInFOV.push_back(enemy);
			}

			break;
		case eEntityType::ITEM:
			{
				seesItem = true;
				vItemsInFOV.push_back(entity);
				break;
			}
		}
	}
	m_pWorldState->SetProperty(WorldStatePropertyKey::SeesEnemy, seesEnemy);
	m_pWorldState->SetProperty(WorldStatePropertyKey::SeesItem, seesItem);
	if (seesItem)
	{
		Elite::Vector2 nearestItem = {99999999999.f,9999999999.f};
		for (unsigned int i = 0; i < vItemsInFOV.size(); ++i)
		{
			if (Elite::Distance(vItemsInFOV[i].Location, agentInfo.Position) < Elite::Distance(nearestItem, agentInfo.Position))
			{
				nearestItem = vItemsInFOV[i].Location;
				m_Nearestitem = vItemsInFOV[i];
			}			
		}
		m_pWorldState->SetProperty(WorldStatePropertyKey::NearestItemPosition, nearestItem);
		m_pWorldState->SetProperty(WorldStatePropertyKey::IsitemInRange, Elite::Distance(agentInfo.Position, nearestItem) < m_pWorldState->GetFloatProperty(WorldStatePropertyKey::GrabRange));
	}
	Elite::Vector2 nearestEnemy = { 99999999999.f,9999999999.f };
	if (seesEnemy)
	{
		for (unsigned int i = 0; i < vEnemiesInFOV.size(); ++i)
		{
			if (Elite::Distance(vEnemiesInFOV[i].Location, agentInfo.Position) < Elite::Distance(nearestEnemy, agentInfo.Position))
			{
				nearestEnemy = vEnemiesInFOV[i].Location;
				m_pWorldState->SetProperty(WorldStatePropertyKey::NearestEnemySize, vEnemiesInFOV[i].Size);
			}
				
		}
		m_pWorldState->SetProperty(WorldStatePropertyKey::NearestEnemyPosition, nearestEnemy);
	}
	if (m_pWorldState->GetBoolProperty(WorldStatePropertyKey::HasGun) &&
		m_pWorldState->GetFloatProperty(WorldStatePropertyKey::GunRange) > Elite::Distance(nearestEnemy, agentInfo.Position))
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::isEnemyInRange, true);
		
		float angle = 0.0f;
		Elite::Vector2 agentDir{ cosf(agentInfo.Orientation - (float)M_PI * 0.5f), sinf(agentInfo.Orientation - (float)M_PI * 0.5f) };
		Elite::Vector2 otherDir = nearestEnemy - agentInfo.Position;
		float dot = Elite::Dot(agentDir, otherDir);
		float magA = agentDir.Magnitude();
		float magB = otherDir.Magnitude();
		angle = (float)acos(dot / (magA * magB));

		if (Elite::Distance(agentInfo.Position, nearestEnemy) * sinf(angle) < m_pWorldState->GetFloatProperty(WorldStatePropertyKey::NearestEnemySize) * 0.75f)
		{
			m_pWorldState->SetProperty(WorldStatePropertyKey::EnemyInFront, true);
		}
		else
		{
			m_pWorldState->SetProperty(WorldStatePropertyKey::EnemyInFront, false);
		}
	}
	else
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::isEnemyInRange, false);
	}
	if (m_pWorldState->GetBoolProperty(WorldStatePropertyKey::HasFood) && m_pWorldState->GetIntProperty(WorldStatePropertyKey::FoodAmount) < (m_pWorldState->GetFloatProperty(WorldStatePropertyKey::MaxEnergy) - agentInfo.Energy))
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::ShoudEat, true);
	}
	else
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::ShoudEat, false);
	}
	if (m_pWorldState->GetBoolProperty(WorldStatePropertyKey::HasMedkit) && m_pWorldState->GetIntProperty(WorldStatePropertyKey::MedKitAmount) < (m_pWorldState->GetFloatProperty(WorldStatePropertyKey::MaxHealth) - agentInfo.Health))
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::ShouldHeal, true);
	}
	else
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::ShouldHeal, false);
	}

	if (!agentInfo.IsInHouse) m_MayRunInHouse = false;
	if (agentInfo.IsInHouse && !m_MayRunInHouse) m_Run = false;

	//ask planner for a plan and execute it
	while (true)
	{
		for (unsigned int i = 0; i < m_pGoals.size(); ++i)
		{
			if (m_pPlanner->Plan(m_pGoals.at(i), m_pPlan, m_pWorldState))
			{
				for (auto action : m_pPlan)
				{
					action->Execute();
					if (m_MustReturn) return m_Output;
				}
			}
		}
	}
}

//This function should only be used for rendering debug elements
void Plugin::Render(float dt) const
{
	//This Render function should only contain calls to Interface->Draw_... functions
	//m_pInterface->Draw_SolidCircle(m_Target, .7f, { 0,0 }, { 1, 0, 0 });
}

void Plugin::Heal()
{
	if (!m_pInterface->Inventory_UseItem(m_MedkitSlot))
	{
		std::cout << "missed call" << std::endl;
	}
	if (!m_pInterface->Inventory_RemoveItem(m_MedkitSlot))
	{
		std::cout << "missed call" << std::endl;
	}
	m_Inventory[m_MedkitSlot] = ItemInfo();
	m_Inventory[m_MedkitSlot].Type = eItemType::GARBAGE;
	m_UsedItemSlots[m_MedkitSlot] = false;
	m_MedkitCount--;
	if (m_MedkitCount > 0)
	{
		for (unsigned int i = 0; i < m_Inventory.size(); ++i)
		{
			if (m_Inventory[i].Type == eItemType::MEDKIT)
			{
				m_MedkitSlot = i;
				m_pWorldState->SetProperty(WorldStatePropertyKey::MedKitAmount, (int)m_pInterface->Item_GetMetadata(m_Inventory[i], "health"));
			}
		}
	}
	else
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::HasMedkit, false);
	}
}

void Plugin::Eat()
{
	if (!m_pInterface->Inventory_UseItem(m_FoodSlot))
	{
		std::cout << "missed call" << std::endl;
	}
	if (!m_pInterface->Inventory_RemoveItem(m_FoodSlot))
	{
		std::cout << "missed call" << std::endl;
	}
	m_Inventory[m_FoodSlot] = ItemInfo();
	m_Inventory[m_FoodSlot].Type = eItemType::GARBAGE;
	m_UsedItemSlots[m_FoodSlot] = false;
	m_FoodCount--;
	if (m_FoodCount > 0)
	{
		for (unsigned int i = 0; i < m_Inventory.size(); ++i)
		{
			if (m_Inventory[i].Type == eItemType::FOOD)
			{
				m_FoodSlot = i;
				m_pWorldState->SetProperty(WorldStatePropertyKey::FoodAmount, (int)m_pInterface->Item_GetMetadata(m_Inventory[i], "energy"));
			}
		}
	}
	else
	{
		m_pWorldState->SetProperty(WorldStatePropertyKey::HasFood, false);
	}
}

void Plugin::Attack()
{
	if (!m_pInterface->Inventory_UseItem(m_PistolSlot))
	{
		std::cout << "missed call" << std::endl;
	}
	m_Ammo--;
	if (m_Ammo == 0)
	{
		if (!m_pInterface->Inventory_RemoveItem(m_PistolSlot))
		{
			std::cout << "missed call" << std::endl;
		}
		m_Inventory[m_PistolSlot] = ItemInfo();
		m_Inventory[m_PistolSlot].Type = eItemType::GARBAGE;
		m_UsedItemSlots[m_PistolSlot] = false;
		m_PistolCount--;
		if (m_PistolCount > 0)
		{
			for (unsigned int i = 0; i < m_Inventory.size(); ++i)
			{
				if (m_Inventory[i].Type == eItemType::PISTOL)
				{
					m_PistolSlot = i;
					m_Ammo = m_pInterface->Item_GetMetadata(m_Inventory[i], "ammo");
					m_pWorldState->SetProperty(WorldStatePropertyKey::GunRange, (float)m_pInterface->Item_GetMetadata(m_Inventory[i], "range"));
				}
			}
		}
		else
		{
			m_pWorldState->SetProperty(WorldStatePropertyKey::HasGun, false);
		}
	}
	m_MustReturn = true;
}

void Plugin::Collect()
{
	bool stored = false;
	if (m_ItemInfo.Type == eItemType::PISTOL)
	{
		for (unsigned int i = 0; i < m_UsedItemSlots.size(); ++i)
		{
			if (!stored && m_UsedItemSlots[i] == false)
			{
				if (!m_pInterface->Inventory_AddItem(i, m_ItemInfo))
				{
					std::cout << "missed call" << std::endl;
				}
				m_UsedItemSlots[i] = true;
				m_Inventory[i] = m_ItemInfo;
				if (m_PistolCount == 0) 
				{
					m_PistolSlot = i;
					m_Ammo = m_pInterface->Item_GetMetadata(m_ItemInfo, "ammo");
					m_pWorldState->SetProperty(WorldStatePropertyKey::GunRange, (float)m_pInterface->Item_GetMetadata(m_ItemInfo, "range"));
					m_pWorldState->SetProperty(WorldStatePropertyKey::HasGun, true);
				}
				m_PistolCount++;
				stored = true;
				break;
			}
		}
		if (!stored)
		{
			if (m_FoodCount >= 3)
			{
				if (!m_pInterface->Inventory_RemoveItem(m_FoodSlot))
				{
					std::cout << "missed call" << std::endl;
				}
				m_FoodCount--;
				if (!m_pInterface->Inventory_AddItem(m_FoodSlot, m_ItemInfo))
				{
					std::cout << "missed call" << std::endl;
				}
				m_Inventory[m_FoodSlot] = m_ItemInfo;
				if (m_PistolCount == 0)
				{
					m_PistolSlot = m_FoodSlot;
					m_Ammo = m_pInterface->Item_GetMetadata(m_ItemInfo, "ammo");
					m_pWorldState->SetProperty(WorldStatePropertyKey::GunRange, (float)m_pInterface->Item_GetMetadata(m_ItemInfo, "range"));
					m_pWorldState->SetProperty(WorldStatePropertyKey::HasGun, true);
				}
				m_PistolCount++; 
				for (unsigned int i = 0; i < m_Inventory.size(); ++i)
				{
					if (m_Inventory[i].Type == eItemType::FOOD)
					{
						m_FoodSlot = i;
						m_pWorldState->SetProperty(WorldStatePropertyKey::FoodAmount, (int)m_pInterface->Item_GetMetadata(m_Inventory[i], "energy"));
						break;
					}
				}
				stored = true;
			}
			if (m_MedkitCount >= 3)
			{
				if (!m_pInterface->Inventory_RemoveItem(m_MedkitSlot))
				{
					std::cout << "missed call" << std::endl;
				}
				m_MedkitCount--;
				if (!m_pInterface->Inventory_AddItem(m_MedkitSlot, m_ItemInfo))
				{
					std::cout << "missed call" << std::endl;
				}
				m_Inventory[m_MedkitSlot] = m_ItemInfo;
				if (m_PistolCount == 0)
				{
					m_PistolSlot = m_MedkitSlot;
					m_Ammo = m_pInterface->Item_GetMetadata(m_ItemInfo, "ammo");
					m_pWorldState->SetProperty(WorldStatePropertyKey::GunRange, (float)m_pInterface->Item_GetMetadata(m_ItemInfo, "range"));
					m_pWorldState->SetProperty(WorldStatePropertyKey::HasGun, true);
				}
				m_PistolCount++;
				for (unsigned int i = 0; i < m_Inventory.size(); ++i)
				{
					if (m_Inventory[i].Type == eItemType::MEDKIT)
					{
						m_MedkitSlot = i;
						m_pWorldState->SetProperty(WorldStatePropertyKey::MedKitAmount, (int)m_pInterface->Item_GetMetadata(m_Inventory[i], "health"));
						break;
					}
				}
				stored = true;
			}
		}
	}
	else if (m_ItemInfo.Type == eItemType::MEDKIT)
	{
		for (unsigned int i = 0; i < m_UsedItemSlots.size(); ++i)
		{
			if (!stored && m_UsedItemSlots[i] == false)
			{
				if (!m_pInterface->Inventory_AddItem(i, m_ItemInfo))
				{
					std::cout << "missed call" << std::endl;
				}
				m_UsedItemSlots[i] = true;
				m_Inventory[i] = m_ItemInfo;
				if (m_MedkitCount == 0)
				{
					m_MedkitSlot = i;
					m_pWorldState->SetProperty(WorldStatePropertyKey::MedKitAmount, (int)m_pInterface->Item_GetMetadata(m_ItemInfo, "health"));
					m_pWorldState->SetProperty(WorldStatePropertyKey::HasMedkit, true);
				}
				m_MedkitCount++;
				stored = true;
				break;
			}
		}
		if (!stored && m_FoodCount >= 3)
		{
			if (!m_pInterface->Inventory_RemoveItem(m_FoodSlot))
			{
				std::cout << "missed call" << std::endl;
			}
			if (!m_pInterface->Inventory_AddItem(m_FoodSlot, m_ItemInfo))
			{
				std::cout << "missed call" << std::endl;
			}
			m_FoodCount--;
			m_Inventory[m_FoodSlot] = m_ItemInfo;
			if (m_MedkitCount == 0)
			{
				m_MedkitSlot = m_FoodSlot;
				m_pWorldState->SetProperty(WorldStatePropertyKey::MedKitAmount, (int)m_pInterface->Item_GetMetadata(m_ItemInfo, "health"));
				m_pWorldState->SetProperty(WorldStatePropertyKey::HasMedkit, true);
			}
			m_MedkitCount++;
			for (unsigned int i = 0; i < m_Inventory.size(); ++i)
			{
				if (m_Inventory[i].Type == eItemType::FOOD)
				{
					m_FoodSlot = i;
					break;
				}
			}
			stored = true;
		}
	}
	else if (m_ItemInfo.Type == eItemType::FOOD)
	{
		for (unsigned int i = 0; i < m_UsedItemSlots.size(); ++i)
		{
			if (!stored && m_UsedItemSlots[i] == false)
			{
				if (!m_pInterface->Inventory_AddItem(i, m_ItemInfo))
				{
					std::cout << "missed call" << std::endl;
				}
				m_UsedItemSlots[i] = true;
				m_Inventory[i] = m_ItemInfo;
				if (m_FoodCount == 0)
				{
					m_FoodSlot = i;
					m_pWorldState->SetProperty(WorldStatePropertyKey::FoodAmount, (int)m_pInterface->Item_GetMetadata(m_ItemInfo, "energy"));
					m_pWorldState->SetProperty(WorldStatePropertyKey::HasFood, true);
				}
				m_FoodCount++;
				stored = true;
				break;
			}
		}
		if (!stored && m_MedkitCount >= 3)
		{
			if (!m_pInterface->Inventory_RemoveItem(m_MedkitSlot))
			{
				std::cout << "missed call" << std::endl;
			}
			if(!m_pInterface->Inventory_AddItem(m_MedkitSlot, m_ItemInfo))
			{
				std::cout << "missed call" << std::endl;
			}
			m_MedkitCount--;
			m_Inventory[m_MedkitSlot] = m_ItemInfo;
			if (m_FoodCount == 0)
			{
				m_FoodSlot = m_MedkitSlot;
				m_pWorldState->SetProperty(WorldStatePropertyKey::FoodAmount, (int)m_pInterface->Item_GetMetadata(m_ItemInfo, "energy"));
				m_pWorldState->SetProperty(WorldStatePropertyKey::HasFood, true);
			}
			m_FoodCount++;
			for (unsigned int i = 0; i < m_Inventory.size(); ++i)
			{
				if (m_Inventory[i].Type == eItemType::MEDKIT)
				{
					m_MedkitSlot = i;
				}
			}
			stored = true;
		}
	}
	if (!stored)
	{
		switch (m_Inventory[0].Type)
		{
		case eItemType::MEDKIT:
			m_MedkitCount--;
			break;
		case eItemType::FOOD:
			m_FoodCount--;
			break;
		case eItemType::PISTOL:
			m_PistolCount--;
			break;
		}
		if (!m_pInterface->Inventory_DropItem(0))
		{
			std::cout << "missed call" << std::endl;
		}
		if (!m_pInterface->Inventory_AddItem(0, m_ItemInfo))
		{
			std::cout << "missed call" << std::endl;
		}
		if (!m_pInterface->Inventory_RemoveItem(0))
		{
			std::cout << "missed call" << std::endl;
		}
		m_UsedItemSlots[0] = false;
	}
	m_pWorldState->SetProperty(WorldStatePropertyKey::IsHoldingItem, false);
	SetNewFinalTarget(m_pWorldState->GetVector2Property(WorldStatePropertyKey::CheckpointLocation));
}

void Plugin::GatherCheckpoint()
{
	//check if checkpoint has changed
	if (m_LastCheckPointLocation != m_pWorldState->GetVector2Property(WorldStatePropertyKey::CheckpointLocation))
	{
		SetNewFinalTarget(m_pWorldState->GetVector2Property(WorldStatePropertyKey::CheckpointLocation));
		m_LastCheckPointLocation = m_pWorldState->GetVector2Property(WorldStatePropertyKey::CheckpointLocation);
	}
	Move();
	m_MustReturn = true;
}

void Plugin::LookAtEnemy()
{
	auto nextTargetPos = m_pWorldState->GetVector2Property(WorldStatePropertyKey::NearestEnemyPosition);
	m_Output.LinearVelocity = nextTargetPos - m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position);
	m_Output.LinearVelocity.Normalize();
	m_Output.LinearVelocity *= m_pWorldState->GetFloatProperty(WorldStatePropertyKey::MaxLinearSpeed);
	m_Output.AutoOrientate = true;
	m_Output.RunMode = m_Run;
	m_MustReturn = true;
}

void Plugin::MoveToItem()
{
	SetNewFinalTarget(m_pWorldState->GetVector2Property(WorldStatePropertyKey::NearestItemPosition));
	Move();
	m_MustReturn = true;
}

void Plugin::MoveToEnemy()
{
	SetNewFinalTarget(m_pWorldState->GetVector2Property(WorldStatePropertyKey::NearestEnemyPosition));
	Move();
	m_MustReturn = true;
}

void Plugin::GrabItem()
{
	m_pInterface->Item_Grab(m_Nearestitem, m_ItemInfo);
	m_pWorldState->SetProperty(WorldStatePropertyKey::IsHoldingItem, true);
}

void Plugin::SetMaxValues(AgentInfo agentInfo)
{
	m_pWorldState->SetProperty(WorldStatePropertyKey::MaxHealth, agentInfo.Health);
	m_pWorldState->SetProperty(WorldStatePropertyKey::MaxEnergy, agentInfo.Energy);
	m_pWorldState->SetProperty(WorldStatePropertyKey::MaxStamina, agentInfo.Stamina);
	m_MaxValuesInitialized = true;
}

void Plugin::SetNewFinalTarget(Elite::Vector2 target)
{
	m_FinalTarget = target;
	m_NotPassedHouses = m_Houses;
}

void Plugin::Move()
{
	auto target = GetNextHouseOnPath();
	auto nextTargetPos = m_pInterface->NavMesh_GetClosestPathPoint(target);
	m_Output.LinearVelocity = nextTargetPos - m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position); //Desired Velocity
	m_Output.LinearVelocity.Normalize(); //Normalize Desired Velocity
	m_Output.LinearVelocity *= m_pWorldState->GetFloatProperty(WorldStatePropertyKey::MaxLinearSpeed); //Rescale to Max Speed
	m_Output.AutoOrientate = true;
	m_Output.RunMode = m_Run;
}

Elite::Vector2 Plugin::GetNextHouseOnPath()
{
	if (m_CheckpointPosOverride)
	{
		if(Elite::Distance(m_pWorldState->GetVector2Property(WorldStatePropertyKey::CheckpointLocation), m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position)) > 1.f)
		{
			m_CheckpointPosOverride = false;
			return m_FinalTarget;
		}
	}

	if (Elite::Distance(m_TimedPos, m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position)) > 1.f)
	{
		timer = 0.f;
		m_TimedPos = m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position);
	}
	if (timer > 3.f)
	{
		m_CheckpointPosOverride = true;
		m_pWorldState->SetProperty(WorldStatePropertyKey::CheckpointLocation,{ m_TimedPos.x + 50, m_TimedPos.y });
	}


	Elite::Vector2 target = m_FinalTarget;

	for (Elite::Vector2 house : m_NotPassedHouses)
	{
		if (Elite::Distance(house, m_FinalTarget) < Elite::Distance(m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position), m_FinalTarget))
		{
			if (Elite::Distance(m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position), house) < Elite::Distance(m_pWorldState->GetVector2Property(WorldStatePropertyKey::Position), target)) target = house;
		}
	}
	if (!m_NotPassedHouses.empty() && target != m_LastTarget)
	{
		if (find(m_NotPassedHouses.begin(), m_NotPassedHouses.end(), m_LastTarget) != m_NotPassedHouses.end())
		{
			m_NotPassedHouses.erase(remove(m_NotPassedHouses.begin(), m_NotPassedHouses.end(), m_LastTarget));
		}
	}
	m_LastTarget = target;
	return target;
}

vector<HouseInfo> Plugin::GetHousesInFOV() const
{
	vector<HouseInfo> vHousesInFOV = {};

	HouseInfo hi = {};
	for (int i = 0;; ++i)
	{
		if (m_pInterface->Fov_GetHouseByIndex(i, hi))
		{
			vHousesInFOV.push_back(hi);
			continue;
		}

		break;
	}

	return vHousesInFOV;
}

vector<EntityInfo> Plugin::GetEntitiesInFOV() const
{
	vector<EntityInfo> vEntitiesInFOV = {};

	EntityInfo ei = {};
	for (int i = 0;; ++i)
	{
		if (m_pInterface->Fov_GetEntityByIndex(i, ei))
		{
			vEntitiesInFOV.push_back(ei);
			continue;
		}

		break;
	}

	return vEntitiesInFOV;
}
