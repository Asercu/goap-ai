#include "stdafx.h"
#include "Planner.h"
#include "Goal.h"
#include "Action.h"


Planner::Planner()
{
	m_pActions = new vector<Action*>();
	m_ActionsAffectingKey = std::map<WorldStatePropertyKey, vector<Action*>>();
}

Planner::~Planner()
{
	for (unsigned int i = 0; i < m_pActions->size(); ++i)
	{
		delete m_pActions->at(i);
	}
	delete m_pActions;
}

void Planner::AddAction(Action* action)
{
	m_pActions->push_back(action);
	for (unsigned int i = 0; i < action->GetEffects()->size(); ++i)
	{
		m_ActionsAffectingKey[action->GetEffects()->at(i)->key].push_back(action);
	}
}

bool Planner::Plan(Goal* goal, vector<Action*>& plan, WorldState* pWorldState)
{
	plan = vector<Action*>();
	vector<WorldStateProperty*> requirements;
	WorldState* localState = new WorldState();

	for (unsigned int i = 0; i < goal->GetRequirements()->size(); ++i)
	{
		if (!PlanForRequirement(goal->GetRequirements()->at(i), pWorldState, localState, plan))
		{
			delete localState;
			for (WorldStateProperty* wsp : requirements)
			{
				delete wsp;
			}
			return false;
		}
	}

	delete localState;
	for (WorldStateProperty* wsp : requirements)
	{
		delete wsp;
	}
	return true;

	/*
	for (unsigned int i = 0; i < goal->GetRequirements()->size(); ++i)
	{
		requirements.push_back(goal->GetRequirements()->at(i));
	}

	for (unsigned int i = 0; i < requirements.size(); ++i)
	{
		bool isOK;
		switch(requirements[i]->type)
		{
		case unset:
			throw std::exception("ERROR: Planner::Plan > type of property not set");
			break;
		case boolean:
			if (!localState.ContainsProperty(requirements[i]->key))
			{
				localState.AddProperty(new WorldStateProperty(requirements[i]->key, pWorldState->GetBoolProperty(requirements[i]->key)));
			}
			isOK = requirements[i]->value.bValue == localState.GetBoolProperty(requirements[i]->key);
			break;
		case integer:
			if (!localState.ContainsProperty(requirements[i]->key))
			{
				localState.AddProperty(new WorldStateProperty(requirements[i]->key, pWorldState->GetIntProperty(requirements[i]->key)));
			}
			isOK = requirements[i]->value.iValue == localState.GetIntProperty(requirements[i]->key);
			break;
		case floatvalue:
			if (!localState.ContainsProperty(requirements[i]->key))
			{
				localState.AddProperty(new WorldStateProperty(requirements[i]->key, pWorldState->GetFloatProperty(requirements[i]->key)));
			}
			isOK = requirements[i]->value.fValue == localState.GetFloatProperty(requirements[i]->key);
			break;
		case vector2:
			if (!localState.ContainsProperty(requirements[i]->key))
			{
				localState.AddProperty(new WorldStateProperty(requirements[i]->key, pWorldState->GetVector2Property(requirements[i]->key)));
			}
			isOK = requirements[i]->value.V2Value == localState.GetVector2Property(requirements[i]->key);
			break;
		}
		if (!isOK)
		{
			deque<Action*> PlanForProperty;
			auto action = FindActionFor(requirements[i]);
			if (action == nullptr) return false;

		}
	}
	*/
}

Action* Planner::FindActionFor(WorldStateProperty* p)
{
	for (Action* action : m_ActionsAffectingKey[p->key])
	{
		bool isOK = true;
		for (unsigned int i = 0; i < action->GetEffects()->size(); ++i)
		{
			if (!PropertiesEqual(action->GetEffects()->at(i), p)) isOK = false;
		}
		if (isOK) return action;
	}
	return nullptr;
}

bool Planner::PropertiesEqual(WorldStateProperty* a, WorldStateProperty* b)
{
	if (a->type != b->type) throw std::exception("ERROR: Planner::PropertiesEqual > properties have different types");
	switch (a->type)
	{
	default:
	case unset:
		throw std::exception("ERROR: Planner::PropertiesEqual > type of property not set");
		break;
	case boolean:
		return a->value.bValue == b->value.bValue;
		break;
	case integer:
		return a->value.iValue == b->value.iValue; 
		break;
	case floatvalue:
		return a->value.fValue == b->value.fValue;
		break;
	case vector2:
		return a->value.V2Value == b->value.V2Value;
		break;
	}
}

bool Planner::PlanForRequirement(WorldStateProperty*pRequirement, WorldState* pWorldState, WorldState* pLocalState, vector<Action*>& plan)
{
	bool isOK;
	switch (pRequirement->type)
	{
	case unset:
		throw std::exception("ERROR: Planner::Plan > type of property not set");
		break;
	case boolean:
		if (!pLocalState->ContainsProperty(pRequirement->key))
		{
			pLocalState->AddProperty(new WorldStateProperty(pRequirement->key, pWorldState->GetBoolProperty(pRequirement->key)));
		}
		isOK = pRequirement->value.bValue == pLocalState->GetBoolProperty(pRequirement->key);
		break;
	case integer:
		if (!pLocalState->ContainsProperty(pRequirement->key))
		{
			pLocalState->AddProperty(new WorldStateProperty(pRequirement->key, pWorldState->GetIntProperty(pRequirement->key)));
		}
		isOK = pRequirement->value.iValue == pLocalState->GetIntProperty(pRequirement->key);
		break;
	case floatvalue:
		if (!pLocalState->ContainsProperty(pRequirement->key))
		{
			pLocalState->AddProperty(new WorldStateProperty(pRequirement->key, pWorldState->GetFloatProperty(pRequirement->key)));
		}
		isOK = pRequirement->value.fValue == pLocalState->GetFloatProperty(pRequirement->key);
		break;
	case vector2:
		if (!pLocalState->ContainsProperty(pRequirement->key))
		{
			pLocalState->AddProperty(new WorldStateProperty(pRequirement->key, pWorldState->GetVector2Property(pRequirement->key)));
		}
		isOK = pRequirement->value.V2Value == pLocalState->GetVector2Property(pRequirement->key);
		break;
	}
	if (!isOK)
	{
		auto action = FindActionFor(pRequirement);
		if (action == nullptr) return false;
		for (unsigned int i = 0; i < action->GetRequirements()->size(); ++i)
		{
			auto req = action->GetRequirements()->at(i);
			if (!PlanForRequirement(req, pWorldState, pLocalState, plan)) return false;
		}
		//set effects in localstate
		for (unsigned int i = 0; i < action->GetEffects()->size(); ++i)
		{
			auto effect = action->GetEffects()->at(i);
			switch (effect->type)
			{
			case unset:
				throw std::exception("ERROR: Planner::Plan > type of property not set");
				break;
			case boolean:
				if (!pLocalState->ContainsProperty(effect->key))
				{
					pLocalState->AddProperty(new WorldStateProperty(effect->key, effect->value.bValue));
				}
				else
				{
					pLocalState->SetProperty(effect->key, effect->value.bValue);
				}
				break;
			case integer:
				if (!pLocalState->ContainsProperty(effect->key))
				{
					pLocalState->AddProperty(new WorldStateProperty(effect->key, effect->value.iValue));
				}
				else
				{
					pLocalState->SetProperty(effect->key, effect->value.iValue);
				}
				break;
			case floatvalue:
				if (!pLocalState->ContainsProperty(effect->key))
				{
					pLocalState->AddProperty(new WorldStateProperty(effect->key, effect->value.fValue));
				}
				else
				{
					pLocalState->SetProperty(effect->key, effect->value.fValue);
				}
				break;
			case vector2:
				if (!pLocalState->ContainsProperty(effect->key))
				{
					pLocalState->AddProperty(new WorldStateProperty(effect->key, effect->value.V2Value));
				}
				else
				{
					pLocalState->SetProperty(effect->key, effect->value.V2Value);
				}
				break;
			}
		}
		plan.push_back(action);
	}
	return true;
}
