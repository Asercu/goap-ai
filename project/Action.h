#pragma once
#include "WorldState.h"

class Plugin;
class Action
{
	friend Plugin;

public:
	Action();
	~Action();

	void AddRequirement(WorldStateProperty* requirement);
	void AddEffect(WorldStateProperty* effect);
	vector<WorldStateProperty*>* GetEffects() { return m_pEffects; };
	vector<WorldStateProperty*>* GetRequirements() { return m_pRequirements; };

	void Execute();

private:
	std::vector<WorldStateProperty*>* m_pRequirements;
	std::vector<WorldStateProperty*>* m_pEffects;
	std::function<void(void)> m_Function;
};

