#include "stdafx.h"
#include "Goal.h"


Goal::Goal()
{
	m_pRequirements = new vector<WorldStateProperty*>();
}


Goal::~Goal()
{
	for (unsigned int i = 0; i < m_pRequirements->size(); ++i)
	{
		delete m_pRequirements->at(i);
	}
	delete m_pRequirements;
}

void Goal::AddRequirement(WorldStateProperty* requirement)
{
	m_pRequirements->push_back(requirement);
}
