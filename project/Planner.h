#pragma once
#include "Action.h"
#include <map>

class Goal;

class Planner
{
public:
	Planner();
	~Planner();

	void AddAction(Action* action);
	bool Plan(Goal* goal, vector<Action*>& plan, WorldState* pWorldState);
	Action* FindActionFor(WorldStateProperty* p);
	bool PropertiesEqual(WorldStateProperty* a, WorldStateProperty* b);
	bool PlanForRequirement(WorldStateProperty*pRequirement,WorldState* pWorldState, WorldState* pLocalState, vector<Action*>& plan);

private:
	vector<Action*>* m_pActions;
	std::map<WorldStatePropertyKey, vector<Action*>> m_ActionsAffectingKey;
};

