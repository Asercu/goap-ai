#pragma once
#include "WorldStateVars.h"

class WorldState;

class Goal
{
public:
	Goal();
	~Goal();

	void AddRequirement(WorldStateProperty* requirement);
	std::vector<WorldStateProperty*>* GetRequirements() { return m_pRequirements; };

private:
	std::vector<WorldStateProperty*>* m_pRequirements;
};

