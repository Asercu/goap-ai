#pragma once
#include "WorldStateVars.h"

class WorldState
{
public:
	WorldState();
	~WorldState();
	void AddProperty(WorldStateProperty* property);
	void SetProperty(WorldStatePropertyKey key, bool bValue);
	void SetProperty(WorldStatePropertyKey key, int iValue);
	void SetProperty(WorldStatePropertyKey key, float fValue);
	void SetProperty(WorldStatePropertyKey key, Elite::Vector2 V2Value);
	bool CheckPoperty(WorldStatePropertyKey key, WorldStatePropertyValue value);
	bool GetBoolProperty(WorldStatePropertyKey key);
	int GetIntProperty(WorldStatePropertyKey key);
	float GetFloatProperty(WorldStatePropertyKey key);
	Elite::Vector2 GetVector2Property(WorldStatePropertyKey key);
	bool ContainsProperty(WorldStatePropertyKey key);

private:
	std::vector<WorldStateProperty*>* m_pProperties;
	WorldStateProperty* GetProperty(WorldStatePropertyKey key);
};

