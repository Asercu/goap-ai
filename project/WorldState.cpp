#include "stdafx.h"
#include "WorldState.h"


WorldState::WorldState()
{
	m_pProperties = new vector<WorldStateProperty*>();
}


WorldState::~WorldState()
{
	for (unsigned int i = 0; i < m_pProperties->size(); ++i)
	{
		delete m_pProperties->at(i);
	}
	delete m_pProperties;
}

void WorldState::AddProperty(WorldStateProperty* property)
{
	if (GetProperty(property->key) != nullptr) throw std::exception("ERROR: WorldState::Addproperty > property with key already exists, duplicate keys not allowed");
	m_pProperties->push_back(property);
}

void WorldState::SetProperty(WorldStatePropertyKey key, bool bValue)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::boolean) throw std::exception("ERROR: WorldState::SetProperty bool > property is not of type bool");
	if (pProperty->readOnly) throw std::exception("ERROR: WorldState::SetProperty > property is read only!");
	pProperty->value.bValue = bValue;
}

void WorldState::SetProperty(WorldStatePropertyKey key, int iValue)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::integer) throw std::exception("ERROR: WorldState::SetProperty int > property is not of type int");
	if (pProperty->readOnly) throw std::exception("ERROR: WorldState::SetProperty > property is read only!");
	pProperty->value.iValue = iValue;
}

void WorldState::SetProperty(WorldStatePropertyKey key, float fValue)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::floatvalue) throw std::exception("ERROR: WorldState::SetProperty float > property is not of type float");
	if (pProperty->readOnly) throw std::exception("ERROR: WorldState::SetProperty > property is read only!");
	pProperty->value.fValue = fValue;
}

void WorldState::SetProperty(WorldStatePropertyKey key, Elite::Vector2 V2Value)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::vector2) throw std::exception("ERROR: WorldState::SetProperty Vector2 > property is not of type Vector2");
	if (pProperty->readOnly) throw std::exception("ERROR: WorldState::SetProperty > property is read only!");
	pProperty->value.V2Value = V2Value;
}

bool WorldState::GetBoolProperty(WorldStatePropertyKey key)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::boolean) throw std::exception("ERROR: WorldState::GetBoolProperty > property is not of type Bool");
	return pProperty->value.bValue;
}

int WorldState::GetIntProperty(WorldStatePropertyKey key)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::integer) throw std::exception("ERROR: WorldState::GetIntProperty > property is not of type int");
	return pProperty->value.iValue;
}

float WorldState::GetFloatProperty(WorldStatePropertyKey key)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::floatvalue) throw std::exception("ERROR: WorldState::GetFloatProperty > property is not of type float");
	return pProperty->value.fValue;
}

Elite::Vector2 WorldState::GetVector2Property(WorldStatePropertyKey key)
{
	auto pProperty = GetProperty(key);
	if (pProperty->type != WorldStatePropertyType::vector2) throw std::exception("ERROR: WorldState::GetVector2Property > property is not of type Vector2");
	return pProperty->value.V2Value;
}

bool WorldState::ContainsProperty(WorldStatePropertyKey key)
{
	auto pProperty = GetProperty(key);
	return pProperty != nullptr;
}

WorldStateProperty* WorldState::GetProperty(WorldStatePropertyKey key)
{
	for (unsigned int i = 0; i < m_pProperties->size(); ++i)
	{
		if (m_pProperties->at(i)->key == key) return m_pProperties->at(i);
	}
	return nullptr;
}
