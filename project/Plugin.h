#pragma once
#include "IExamPlugin.h"
#include "Exam_HelperStructs.h"
#include "Planner.h"
#include "Goal.h"
#include "Action.h"
#include "WorldState.h"

class IBaseInterface;
class IExamInterface;

class Plugin :public IExamPlugin
{
public:
	Plugin() {};
	virtual ~Plugin() {};

	void Initialize(IBaseInterface* pInterface, PluginInfo& info) override;
	void DllInit() override;
	void DllShutdown() override;

	void InitGameDebugParams(GameDebugParams& params) override;
	void ProcessEvents(const SDL_Event& e) override;

	SteeringPlugin_Output UpdateSteering(float dt) override;
	void Render(float dt) const override;

	//Actions
	void Heal();
	void Eat();
	void Attack();
	void Collect();
	void GatherCheckpoint();
	void LookAtEnemy();
	void MoveToItem();
	void MoveToEnemy();
	void GrabItem();

private:
	SteeringPlugin_Output m_Output;
	bool m_MustReturn;

	//GOAP
	bool m_MaxValuesInitialized = false;
	void SetMaxValues(AgentInfo agentInfo);
	Planner* m_pPlanner;
	WorldState* m_pWorldState;
	vector<Goal*> m_pGoals;
	vector<Action*> m_pPlan;

	//bot variables and functions
	vector<Elite::Vector2> m_Houses;
	vector<Elite::Vector2> m_NotPassedHouses;
	bool m_MayRunInHouse = false;
	bool m_Run = false;
	Elite::Vector2 m_LastCheckPointLocation = { 0,0 };
	Elite::Vector2 m_FinalTarget = { 0,0 };
	void SetNewFinalTarget(Elite::Vector2 target);
	void Move();
	Elite::Vector2 GetNextHouseOnPath();
	Elite::Vector2 m_LastTarget = { 0,0 };
	EntityInfo m_Nearestitem;
	ItemInfo m_ItemInfo;
	std::vector<bool> m_UsedItemSlots = { false, false, false, false, false };
	std::vector<ItemInfo> m_Inventory = { ItemInfo(), ItemInfo(), ItemInfo(), ItemInfo(), ItemInfo() };
	unsigned int m_PistolSlot;
	unsigned int m_MedkitSlot;
	unsigned int m_FoodSlot;
	unsigned int m_Ammo;
	unsigned int m_PistolCount;
	unsigned int m_MedkitCount;
	unsigned int m_FoodCount;

	//try fix navmesh bug
	float timer = 0;
	Elite::Vector2 m_TimedPos{};
	bool m_CheckpointPosOverride = false;

	//Interface, used to request data from/perform actions with the AI Framework
	IExamInterface* m_pInterface = nullptr;
	vector<HouseInfo> GetHousesInFOV() const;
	vector<EntityInfo> GetEntitiesInFOV() const;	
};

//ENTRY
//This is the first function that is called by the host program
//The plugin returned by this function is also the plugin used by the host program
extern "C"
{
	__declspec (dllexport) IPluginBase* Register()
	{
		return new Plugin();
	}
}