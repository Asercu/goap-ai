#pragma once
#include "EliteMath/EVector2.h"

enum class WorldStatePropertyKey
{
	HasFood,
	FoodAmount,
	Currenthealth,
	MaxHealth,
	HasMedkit,
	MedKitAmount,
	CurrentEnergy,
	MaxEnergy,
	CurrentStamina,
	MaxStamina,
	HasGun,
	SeesEnemy,
	SeesItem,
	SeesHouse,
	IsInHouse,
	IsHoldingItem,
	NearestItemPosition,
	NearestEnemyPosition,
	isEnemyInRange,
	IsitemInRange,
	GunRange,
	ShoudEat,
	ShouldHeal,
	NearestEnemySize,
	EnemyInFront,
	LinearVelocity,
	AngularVelocity,
	CurrentLinearSpeed,
	Position,
	Orientation,
	MaxLinearSpeed,
	MaxAngularSpeed,
	GrabRange,
	AgentSize,
	IsBitten,
	CheckpointLocation,
	hasHealed,
	hasEaten,
	HasAttacked,
	itemCollected,
	checkPointgathered,
	NearestItemHash,
};

enum WorldStatePropertyType
{
	unset,
	boolean,
	integer,
	floatvalue,
	vector2
};

union WorldStatePropertyValue
{
	bool bValue;
	int iValue;
	float fValue;
	Elite::Vector2 V2Value;
	~WorldStatePropertyValue() {};
};

struct WorldStateProperty
{
	WorldStatePropertyKey key;
	WorldStatePropertyType type = unset;
	WorldStatePropertyValue value{ 0 };
	bool readOnly;

	WorldStateProperty(WorldStatePropertyKey k, bool v, bool isReadOnly = false)
	{
		key = k;
		type = WorldStatePropertyType::boolean;
		value.bValue = v;
		readOnly = isReadOnly;
	}
	WorldStateProperty(WorldStatePropertyKey k, int v, bool isReadOnly = false)
	{
		key = k;
		type = WorldStatePropertyType::integer;
		value.iValue = v;
		readOnly = isReadOnly;
	}
	WorldStateProperty(WorldStatePropertyKey k, float v, bool isReadOnly = false)
	{
		key = k;
		type = WorldStatePropertyType::floatvalue;
		value.fValue = v;
		readOnly = isReadOnly;
	}
	WorldStateProperty(WorldStatePropertyKey k, Elite::Vector2 v, bool isReadOnly = false)
	{
		key = k;
		type = WorldStatePropertyType::vector2;
		value.V2Value = v;
		readOnly = isReadOnly;
	}

	bool operator==(const WorldStateProperty& rhs) const
	{
		switch (type)
		{
			case WorldStatePropertyType::boolean:
				return value.bValue == rhs.value.bValue;
			case WorldStatePropertyType::integer:
				return value.iValue == rhs.value.iValue;
			case WorldStatePropertyType::floatvalue:
				return value.fValue == rhs.value.fValue;
			case WorldStatePropertyType::vector2:
				return value.V2Value == rhs.value.V2Value;
			case WorldStatePropertyType::unset:
				throw std::exception("WorldStateProperty::Operator== > property not set");
			default:
				return false;
		}
	}
};